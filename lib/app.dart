import 'package:flutter/material.dart';
import 'package:validators/validators.dart';
import 'package:date_field/date_field.dart';
import 'package:country_picker/country_picker.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login me',
      home: Scaffold(
        body: LoginScreen(),
      ),
    );
  }

}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}



class LoginState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String name;
  late String lastmiddlename;
  late String birthyear;
  late String address;
  late String province;
  late String district;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            emailField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            middlelastField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            nameField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            birthdayField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            countryField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            provinceField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            districtField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            loginButton()
          ],
        ),
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          labelText: 'Email address'
      ),
      validator: (value) {
        if (!value!.contains('@')) {
          return "Input valid email.";
        }
        return null;
      },

      onSaved: (value) {
        email = value as String;
      },

    );
  }

  Widget middlelastField() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Middle and last name'
      ),
    validator: (value) {
        if(value!.isEmpty == true) {
          return "Please enter middle and last name!";
        }
        return null;
    },

      onSaved: (value){
        lastmiddlename = value as String;
      }
    );
  }

  Widget nameField() {
    return TextFormField(
        decoration: InputDecoration(
            labelText: 'Name'
        ),
        validator: (value) {
          if(value!.isEmpty == true) {
            return "Please enter name!";
          }
          return null;
        },

        onSaved: (value){
          name = value as String;
        }
    );
  }

  Widget birthdayField() {
    return DateTimeFormField(
      decoration: InputDecoration(
        labelText: 'Birthday'
      ),
      validator: (value) {
        if(value! == null)
          return "Fill your birthday!";
      },
    );
  }

  Widget countryField() {
    String? valueChoose;
    List listItem = [
      "Vietnam", "Korea", "Japan", "China", "Cambodia", "Philippine"
    ];
    return DropdownButton(
        hint: Text("Select country"),
        isExpanded: true,
        value: valueChoose,
        onChanged: (newValue) {
          setState(() {
            valueChoose = newValue as String?;

          });
        },
        items: listItem.map((valueItem) {
          return DropdownMenuItem(
            value: valueItem,
            child: Text(valueItem),
              );
        }).toList(),
    );
  }


  Widget provinceField() {
    return TextFormField(
        decoration: InputDecoration(
            labelText: 'Province'
        ),
        validator: (value) {
          if(value!.isEmpty == true) {
            return "Please enter province!";
          }
          return null;
        },

        onSaved: (value){
          province = value as String;
        }
    );
  }

  Widget districtField() {
    return TextFormField(
        decoration: InputDecoration(
            labelText: 'District'
        ),
        validator: (value) {
          if(value!.isEmpty == true) {
            return "Please enter name!";
          }
          return null;
        },

        onSaved: (value){
          district = value as String;
        }
    );
  }


  Widget loginButton() {

    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();

            print('email=$email');
          }
        },
        child: Text('Login')
    );
  }
}